<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'product';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $fillable = [
    'name','price','img'
    ];
}
