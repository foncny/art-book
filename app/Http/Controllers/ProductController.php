<?php

namespace App\Http\Controllers;
use App\Product;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        // dd($products);
        return view('product.index')->with('products',$products);
        // $products = Product::all();
        // return view ('product.index')->with('product',$products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request->session()->forget('status');
        return view('product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
        'product_name' => 'required|max:100',
        'price' => 'required',
        'img'=>'required'
        ]);

        $path =$request->img->store('public/products');
        $replace_patth = str_replace("public", "storage",$path);
        //dd($path); 




        $product = new Product;

        $product->name = $request->product_name;
        $product->price = $request->price;
        // $product->detail = $request->detail;
        $product->img = $replace_patth;

        if($product->save()) {
            $request->session()->flash('status','เพิ่ม'.$request->product_name.'สำเร็จ!');
            return redirect('product');
        } else {
          $request->session()->flash('status','เพิ่ม'.$request->product_name.'ไม่สำเร็จ');
            return view('product.create');
        }

     }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
