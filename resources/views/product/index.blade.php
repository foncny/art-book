@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <a href="{{route('product.create')}}" class="btn btn-success">เพิ่มสินค้า</a>
                     <hr>
                     <table class="table table-hover table-dark">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">ชื่อสินค้า</th>
      <th scope="col">ราคา</th>
      <th scope="col">รูปภาพ</th>
    </tr>
  </thead>
  <tbody>
      @foreach($products as $value)

    <tr>
     <th scope="row">1</th>

      <td>{{$value->name}}</td>
      <td>{{$value->price}}</td>
      <td>
      @if($value->img)
      <img src="{{asset($value->img) }}" style="width: 100px;">
      @else
      ไม่มีรูปภาพ

      @endif
  </td>
      
    </tr>
        @endforeach
  </tbody>
</table>


                    
                  
                 


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
