@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row">
		 <div class="col-md-12">
            <div class="card">
                <h4 class="card-header text-center">เพิ่มสินค้า</h4>
					<form style="padding: 15px" method="post" action="{{route('product.store')}}" enctype="multipart/form-data">
						{{ csrf_field () }}	
						@if (session('status'))
     					<div class="alert alert-danger">
								{{session('status')}}
							</div>
						@endif

						 @if ($errors->any())
							    <div class="alert alert-danger">
							        <ul>
							            @foreach ($errors->all() as $error)
							                <li>{{ $error }}</li>
							            @endforeach
							        </ul>
							    </div>
						@endif

		  <div class="form-group">
		  	<div class="col-md-12">
		    <label for="NameOfProduct" >ชื่อสินค้า</label>
		    <input type="text" class="form-control" name="product_name" placeholder="ใส่ชื่อสินค้า" >
		   
		    </div>
		    <div class="form-group">
		  	<div class="col-md-12">
		    <label for="PriceOfProduct">ราคาสินค้า</label>
		    <input type="number" class="form-control" name="price"  placeholder="ใส่ราคา"  >
		  
		    </div>
		 
	
			<div class="form-group">
			    <div class="col-md-12">

			    <label >อัพโหลดรูปสินค้า</label>
			    <input type="file" class="form-control-file" name="img">
			  </div>
			</div>
			<div class="col-md-12">
			  <button type="submit" class="btn btn-primary">บันทึก</button>
			</div>
					</form>
				</div>
			</div>
		</div>
	</div>

@endsection