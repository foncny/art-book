
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>ART BOOK</title>

  <!-- Bootstrap core CSS -->
  <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="{{asset('css/heroic-features.css')}}" rel="stylesheet">

</head>

<body>

 <!-- Page Content -->
<div class="container">

  <!-- Page Heading -->
  <h1 class="my-4 text-center">Art Book
    <!-- <small>Secondary Text</small> -->
  </h1>

  <div class="row">
    <div class="col-lg-6 mb-4">
      <div class="card h-100">
        <a href="#"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
        <div class="card-body">
          <h4 class="card-title text-center">
            <a href="{{route('register','type=buyer')}}">สำหรับผู้ซื้อ</a>
          </h4>
          <p class="card-text">สำหรับผู้ต้องการซื้อภาพวาด สามารถติดต่อผู้ขายได้โดยตรง</p>
        </div>
      </div>
    </div>
    <div class="col-lg-6 mb-4">
      <div class="card h-100">
        <a href="#"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
        <div class="card-body">
          <h4 class="card-title text-center">
            <a href="{{route('register','type=saler')}}">สำหรับผู้ขาย</a>
          </h4>
          <p class="card-text">สำหรับผู้ต้องการขายภาพวาด สามารถขายและแลกเปลี่ยนกับผู้ซื้อได้ทันที</p>
        </div>
      </div>
    </div>
  
    </div>
  </div>
  <!-- /.row -->

  <!-- Pagination -->

<!-- /.container -->

  <!-- Footer -->
 

  <!-- Bootstrap core JavaScript -->
  <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

</body>

</html>
